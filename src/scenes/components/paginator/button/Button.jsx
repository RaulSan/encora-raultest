import './Button.css';

const Button = ({children,handleBtn})=>{
    return (
        <button onClick={handleBtn}>{children}</button>
    )
}

export default Button