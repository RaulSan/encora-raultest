import { useEffect, useState } from 'react';
import './Dots.css';

const Dots = ({paginatorInfo})=>{
    const [dots,setDots] = useState({
        dotNumber:[],
        currentPage:0
    })

    useEffect(()=>{
        setDots(
            {
                dotNumber:new Array(paginatorInfo.pages).fill(0),
                currentPage:paginatorInfo.next ? 
                            paginatorInfo.next.split('=')[1] - 2 :
                            paginatorInfo.pages - 1
            })
    },[paginatorInfo.pages,paginatorInfo.next])

    return (
        <div className="dots-wapper">
            {dots.dotNumber.length && dots.dotNumber.map((_,i)=>{
                return(
                    <span key={i} className={"dot" + (i === dots.currentPage ? ' blue' : '')}></span>
                )
            })
            }
        </div>
    )
}

export default Dots