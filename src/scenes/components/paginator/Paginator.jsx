import './Paginator.css';
import Dots from '../paginator/dots/Dots';
import Button from '../paginator/button/Button';


const Paginator = ({paginatorInfo,handleNext,handleSub})=>{


    return (
      
        <div className="paginator">
            {paginatorInfo && 
            <>
                <div className="paginator-dots">
                        <Dots paginatorInfo={paginatorInfo}></Dots>
                </div>
            
                <div className="paginator-btn">
                    {paginatorInfo.prev && 
                    <Button handleBtn={handleSub}>Previous</Button>
                    }
                    {paginatorInfo.next && 
                        <Button handleBtn={handleNext}>Next</Button>
                    }
                </div>
            </>
            }
        </div>
    )
}

export default Paginator