import React, { useEffect, useState } from 'react';
import Cards from './cards/Cards';
import Paginator from '../components/paginator/Paginator';

import './Season.css';

const Season = ()=>{

    const [card, setCard] = useState({
        results:[]
    });

    useEffect(()=>{
        if(!card.results.length){
            fetch("https://rickandmortyapi.com/api/episode")
            .then(res => res.json())
            .then(data => setCard(data))
         }
    },[card,card.results]);

    const handleNext = ()=>{
        fetch(card.info.next)
        .then(res => res.json())
        .then(data => setCard(data))
    }

    const handleSub = ()=>{
        fetch(card.info.prev)
        .then(res => res.json())
        .then(data => setCard(data))
    }

    return (
        <div className='container'>
            {
                card &&
                card.results.map((data, i ,arr)=>{
                    return(
                        <section key={i}  className="episode-wrapper">
                            <div className="episode-header">
                                <h1 className="title">Episode {data.id} - {data.name} ({data.episode})</h1>
                                <p className="subtitle">{data.air_date}</p>   
                            </div>
                                <Cards  characters={data.characters} ></Cards> 
                            
                           
                            { !(i === arr.length - 1) ?
                            <div className="divider">
                                <span  className="divider-line"></span>
                            </div>
                            : <></>
                            }

                        </section> 
                    )
                })
           
                
            }
            {card &&
              <Paginator handleSub={handleSub} handleNext={handleNext} setCard={setCard} paginatorInfo={card.info}></Paginator>
            }
               
       </div>
    )
}

export default Season;