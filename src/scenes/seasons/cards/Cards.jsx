import React,{ useEffect,useState } from 'react';
import './Cards.css'; 

const Card = ({characters})=>{
    
    const [images, setImages] = useState([])
    const skeleton = new Array(10).fill(0);

    useEffect(()=>{
        characters.map((data)=>fetch(data)
            .then(res => res.json())
            .then(dataImg => setImages( (pastImage)=>[...pastImage,dataImg.image])))
            return(
                setImages([])
            )
    },[characters]);

    return (
        <div className="card-wapper">
            {images.length ?
                images.map((image,i)=>{
                    return(
                            <img className="card-image" key={i} src={image} alt={image}/>
                    )
                }):
                skeleton.map((_,i)=>{
                    return(
                    <div key={i} className="skeleton"></div>
                    )
                })
            }
        </div>
    )
}

export default Card;